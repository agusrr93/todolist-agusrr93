
var data = (localStorage.getItem('toshopList')) ? JSON.parse(localStorage.getItem('toshopList')):{
  toshop: [],
  status:[]
};


var removeSVG = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve"><rect class="noFill" width="22" height="22"/><g><g><path class="fill" d="M16.1,3.6h-1.9V3.3c0-1.3-1-2.3-2.3-2.3h-1.7C8.9,1,7.8,2,7.8,3.3v0.2H5.9c-1.3,0-2.3,1-2.3,2.3v1.3c0,0.5,0.4,0.9,0.9,1v10.5c0,1.3,1,2.3,2.3,2.3h8.5c1.3,0,2.3-1,2.3-2.3V8.2c0.5-0.1,0.9-0.5,0.9-1V5.9C18.4,4.6,17.4,3.6,16.1,3.6z M9.1,3.3c0-0.6,0.5-1.1,1.1-1.1h1.7c0.6,0,1.1,0.5,1.1,1.1v0.2H9.1V3.3z M16.3,18.7c0,0.6-0.5,1.1-1.1,1.1H6.7c-0.6,0-1.1-0.5-1.1-1.1V8.2h10.6V18.7z M17.2,7H4.8V5.9c0-0.6,0.5-1.1,1.1-1.1h10.2c0.6,0,1.1,0.5,1.1,1.1V7z"/></g><g><g><path class="fill" d="M11,18c-0.4,0-0.6-0.3-0.6-0.6v-6.8c0-0.4,0.3-0.6,0.6-0.6s0.6,0.3,0.6,0.6v6.8C11.6,17.7,11.4,18,11,18z"/></g><g><path class="fill" d="M8,18c-0.4,0-0.6-0.3-0.6-0.6v-6.8c0-0.4,0.3-0.6,0.6-0.6c0.4,0,0.6,0.3,0.6,0.6v6.8C8.7,17.7,8.4,18,8,18z"/></g><g><path class="fill" d="M14,18c-0.4,0-0.6-0.3-0.6-0.6v-6.8c0-0.4,0.3-0.6,0.6-0.6c0.4,0,0.6,0.3,0.6,0.6v6.8C14.6,17.7,14.3,18,14,18z"/></g></g></g></svg>';

rendertoshopList();

document.getElementById('add').addEventListener('click', function() {
  var value = document.getElementById('item').value;
  if (value) {
    addItem(value);
  }
});

document.getElementById('item').addEventListener('keydown', function (e) {
  var value = this.value;
  if ((e.code === 'Enter' || e.code === 'NumpadEnter') && value) {
    addItem(value);
  }
});

function addItem (value) {
  addItemtoshopM(value,false);
  document.getElementById('item').value = '';

  data.toshop.push(value);
  data.status.push(false);
  dataObjectUpdated();
}

function rendertoshopList() {
  if (!data.toshop.length) return;

  for (var i = 0; i < data.toshop.length; i++) {
    var value = data.toshop[i];
    var status=data.status[i];
    addItemtoshopM(value,status);
  }
}

function dataObjectUpdated() {
  localStorage.setItem('toshopList', JSON.stringify(data));
}


function removeItem() {
  var item = this.parentNode.parentNode;
  var parent = item.parentNode;
  
  var id = parent.id;
  var value = item.innerText;

  if (id === 'toshop') {
    data.toshop.splice(data.toshop.indexOf(value), 1);
    data.status.splice(data.toshop.indexOf(value), 1);
  }   
  dataObjectUpdated();

  parent.removeChild(item);
}

function removeDom(){
  var html = document.getElementById('editableform');
  while(html.firstChild){
    html.removeChild(html.firstChild);
  }
}

function editableButton(text){
    var listing=document.getElementById('inputForm').value;

    var item=document.getElementById('toshop')

    var id=item.id
    
    if (id === 'toshop') {
      data.toshop.splice(data.toshop.indexOf(text), 1,listing);
    }

    dataObjectUpdated();
    
    //ilangin child editable form
    var html = document.getElementById('editableform');
    while(html.firstChild){
      html.removeChild(html.firstChild);
    }

    //ilangin child toshop
    var html = document.getElementById('toshop');
    while(html.firstChild){
        html.removeChild(html.firstChild);
    }
    rendertoshopList();
    
}
function editItem(texter){
  
  var html = document.getElementById('editableform');
  while(html.firstChild){
    html.removeChild(html.firstChild);
  }

  var listing =document.getElementById('editableform');
  
  var text = document.createElement('p');
  text.innerHTML=`FORM EDIT DATA : `;
  text.classList.add('text');
  var buttonAdd=document.createElement('button');
  buttonAdd.addEventListener('click',function() { editableButton(texter); })
  buttonAdd.classList.add('addButton')

  var buttonClose=document.createElement('button')
  buttonClose.classList.add('closeButton')
  buttonClose.addEventListener('click',removeDom)
  var form = document.createElement('input');
  form.value=texter
  form.setAttribute("id", "inputForm");

  buttonAdd.innerHTML = '<i class="fa fa-check"></i>';
  buttonClose.innerHTML='<i class="fa fa-window-close" aria-hidden="true"></i>';
  form.classList.add('formList');
  listing.appendChild(text)
  listing.appendChild(form)
  listing.appendChild(buttonAdd)
  listing.appendChild(buttonClose)
 
}

function doneItem(text){
    var item=document.getElementById('toshop')

    var id=item.id
    
    if (id === 'toshop') {
      data.status.splice(data.toshop.indexOf(text), 1,true);
    }

    Swal.fire({
      title: 'Yakin sudah belanja barang ini?',
      text: "list belanjaan ini akan ditandai selesai",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya , selesaikan!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Completed!',
          'Belanjaanmu sudah ditandai.',
          'success'
        )
        dataObjectUpdated();

        var html = document.getElementById('toshop');
        while(html.firstChild){
            html.removeChild(html.firstChild);
        }
        rendertoshopList();
      }
    })

    
}

function undoneItem(text){
  var item=document.getElementById('toshop')

  var id=item.id
  
  if (id === 'toshop') {
    data.status.splice(data.toshop.indexOf(text), 1,false);
  }

  Swal.fire({
    title: 'Lupa belanja barang ini?',
    text: "ok , barang ini akan tersedia lagi di list belanja",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya , kembalikan!'
  }).then((result) => {
    if (result.value) {
      Swal.fire(
        'Completed!',
        'Belanjaanmu sudah dikembalikan.',
        'success'
      )
      dataObjectUpdated();

      var html = document.getElementById('toshop');
      while(html.firstChild){
          html.removeChild(html.firstChild);
      }
      rendertoshopList();
    }
  })
  
}

function addItemtoshopM(text, completed) {
  
  var list =document.getElementById('toshop');

  var item = document.createElement('li');
  
  if(completed==false){
    item.classList.add('undone')
  }
  else{
    item.classList.add('done')
  }
  
  item.innerText = text;

  var buttons = document.createElement('div');
  buttons.classList.add('buttons');

  var remove = document.createElement('button');
  remove.classList.add('remove');
  remove.innerHTML = '<i class="fa fa-trash" aria-hidden="true"></i>';

  remove.addEventListener('click', removeItem);
  
  var edit = document.createElement('button');
  edit.classList.add('remove');
  edit.innerHTML='<i class="fa fa-edit"></i>'

  edit.addEventListener('click',function() { editItem(text); } )

  var done = document.createElement('button');
  done.classList.add('remove');
  if(completed==false){
    done.innerHTML='<i class="fa fa-check-circle"></i>'
    done.addEventListener('click',function() { doneItem(text); } )
  }
  else if(completed==true){
    done.innerHTML='<i class="fa fa-undo"></i>'
    done.addEventListener('click',function() { undoneItem(text); } )
  }

  buttons.appendChild(remove);
  buttons.appendChild(edit);
  buttons.appendChild(done);

  item.appendChild(buttons);

  list.insertBefore(item, list.childNodes[0]);
}
